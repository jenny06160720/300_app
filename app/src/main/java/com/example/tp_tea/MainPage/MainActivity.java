package com.example.tp_tea.MainPage;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.tp_tea.R;

public class MainActivity extends AppCompatActivity implements MainContract.MainView {
    // set the variable
    private TextView welcome;
    private MainPresenter mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // set the data in welcome variable
        welcome = (TextView) findViewById(R.id.welcome);

        // Set presenter
        mainPresenter = new MainPresenter(this, this);

        // when the welcome id onclick
        welcome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // call the presenter to go to branch page
                mainPresenter.goToBranch();
            }
        });

    }

    @Override
    public void goToBranch(Intent intent) {
        // change the view
        startActivity(intent);
    }
}