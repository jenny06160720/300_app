package com.example.tp_tea.MainPage;

import android.content.Intent;

public interface MainContract {
    // this is control all presenter and view
    public interface Presenter {
        void goToBranch();
    }

    public interface MainView {
        void goToBranch(Intent intent);
    }
}
