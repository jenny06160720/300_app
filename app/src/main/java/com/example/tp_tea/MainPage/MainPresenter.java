package com.example.tp_tea.MainPage;

import android.content.Context;
import android.content.Intent;

import com.example.tp_tea.*;
import com.example.tp_tea.ui.menu.*;


public class MainPresenter implements MainContract.Presenter {
    // set variable
    public MainContract.MainView mainView;
    public Context context;

    public MainPresenter(MainContract.MainView mainView, Context context) {
        this.mainView = mainView;
        this.context = context;
    }

    @Override
    public void goToBranch() {
        // go to other view
        Intent goToBranch = new Intent(context, Tea_Navigation.class);
        mainView.goToBranch(goToBranch);
    }
}
