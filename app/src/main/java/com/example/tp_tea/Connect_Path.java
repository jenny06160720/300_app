package com.example.tp_tea;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public interface Connect_Path {
    public Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://10.0.2.2:3000")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    public Retrofit retrofit2 = new Retrofit.Builder()
            .baseUrl("http://192.168.1.140:3000")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
}
