package com.example.tp_tea.UI_Test;

import android.content.Context;
import android.content.Intent;

import com.example.tp_tea.MainPage.*;

public class WelcomePresenter implements WelcomeContract.Presenter {
    // set the variable
    public WelcomeContract.MainView view;
    public Context context;

    public WelcomePresenter(WelcomeContract.MainView view, Context context) {
        this.view = view;
        this.context = context;
    }

    @Override
    public void goToMainPage() {
        // set the intent to the main page
        Intent gotoMainPage = new Intent(context, MainActivity.class);
        view.goToMainPage(gotoMainPage);

    }
}
