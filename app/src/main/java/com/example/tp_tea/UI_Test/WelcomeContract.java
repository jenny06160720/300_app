package com.example.tp_tea.UI_Test;

import android.content.Intent;

public class WelcomeContract {
    // this is control all presenter and view
    public interface Presenter {
        void goToMainPage();
    }

    public interface MainView {
        void goToMainPage(Intent intent);
    }
}
