package com.example.tp_tea.UI_Test;

import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.tp_tea.R;

public class WelcomeActivity extends AppCompatActivity implements Animation.AnimationListener,
        WelcomeContract.MainView {
    // set the variable
    private WelcomePresenter presenter;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        //Cancel the ActionBar
        getSupportActionBar().hide();

        // Cancel the state
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        imageView = (ImageView) findViewById(R.id.imageView);

        //imageView setting the animation event
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.welcome_animation);
        animation.setFillEnabled(true);
        animation.setFillAfter(true);
        animation.setAnimationListener(this);
        imageView.setAnimation(animation);

        // set the presenter
        presenter = new WelcomePresenter(this, this);
    }

    @Override
    public void onAnimationStart(Animation animation) {
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        // call the presenter to set change view
        presenter.goToMainPage();
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
    }

    @Override
    public void goToMainPage(Intent intent) {
        // go to main page
        startActivity(intent);
        finish();
    }
}