package com.example.tp_tea.ui.Branch;

import com.google.android.gms.maps.model.LatLng;
import com.example.tp_tea.*;

import org.jetbrains.annotations.TestOnly;

public class BranchPresenter implements BranchContact.Presenter {
    // set the variable
    public BranchContact.mapView mapsView;
    public BranchContact.Model branchRepository;
    public MessageModel messageModel;

    // set the constructors
    public BranchPresenter(BranchContact.mapView mapsView, BranchContact.Model branchRepository, MessageModel messageModel) {
        this.mapsView = mapsView;
        this.branchRepository = branchRepository;
        this.messageModel = messageModel;
    }

    @TestOnly
    public String getTitle() {
        return messageModel.getTitle();
    }

    @TestOnly
    public String getMsg() {
        return messageModel.getMsg();
    }

    @Override
    public void onMapReady() {
        branchRepository.loadLocation(this);
    }

    @Override
    public void getLocation(String licensePlate, Location location) {
        // set the location maker to map
        LatLng locationToLatLng = new LatLng(location.getLat(), location.getLng());
        mapsView.addScooter(licensePlate, locationToLatLng);
        mapsView.scrollMapTo(locationToLatLng);
    }

    @Override
    public void showMessage(String title, int code) {
        // set the message
        messageModel.setTitle(title);
        messageModel.setMsg(messageModel.checkCodeNum(code));
        if (messageModel.getMsg().isEmpty() || messageModel.getTitle().isEmpty()) {
            return;
        }
        mapsView.showMessage(messageModel.getTitle(), messageModel.getMsg());
    }
}
