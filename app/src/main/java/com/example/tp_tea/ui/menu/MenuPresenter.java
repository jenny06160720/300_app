package com.example.tp_tea.ui.menu;

import android.util.Log;

import com.example.tp_tea.MessageModel;

import org.jetbrains.annotations.TestOnly;

import java.util.List;

public class MenuPresenter implements MenuContact.Presenter {
    // set variable
    public MenuContact.menuView view;
    public MenuContact.Model repository;
    public MessageModel messageModel;

    public MenuPresenter(MenuContact.menuView view, MenuContact.Model repository, MessageModel messageModel) {
        this.view = view;
        this.repository = repository;
        this.messageModel = messageModel;
    }

    @TestOnly
    public String getTitle() {
        return messageModel.getTitle();
    }

    @TestOnly
    public String getMsg() {
        return messageModel.getMsg();
    }

    @Override
    public void CallAPI() {
        // call the repository to get the data list
        repository.loadMenus(this);
    }

    @Override
    public void GetResponseSuccess(List<Menu> datas) {
        // call the add to view to add the record on the view
        view.addToView(datas);
    }

    @Override
    public void ShowMessage(String title, int code) {
        // set the message
        messageModel.setTitle(title);
        messageModel.setMsg(messageModel.checkCodeNum(code));
        if (messageModel.getMsg().isEmpty() || messageModel.getTitle().isEmpty()) {
            return;
        }
        view.ShowMessage(messageModel.getTitle(), messageModel.getMsg());
    }
}
