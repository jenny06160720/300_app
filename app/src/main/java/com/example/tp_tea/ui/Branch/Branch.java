package com.example.tp_tea.ui.Branch;

import com.google.gson.annotations.SerializedName;

public class Branch {
    // the branch class is for saving the data from url by api
    @SerializedName("_id")
    public String ID;
    @SerializedName("enlocation")
    public String enlocation;
    @SerializedName("chlocation")
    public String chlocation;
    @SerializedName("lat")
    public String lat;
    @SerializedName("lng")
    public String lng;

    public Branch(String ID, String enlocation, String chlocation, String lat, String lng) {
        this.ID = ID;
        this.enlocation = enlocation;
        this.chlocation = chlocation;
        this.lat = lat;
        this.lng = lng;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getEnlocation() {
        return enlocation;
    }

    public void setEnlocation(String enlocation) {
        this.enlocation = enlocation;
    }

    public String getChlocation() {
        return chlocation;
    }

    public void setChlocation(String chlocation) {
        this.chlocation = chlocation;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }
}
