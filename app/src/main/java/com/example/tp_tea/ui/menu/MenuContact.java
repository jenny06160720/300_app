package com.example.tp_tea.ui.menu;

import com.example.tp_tea.ui.Branch.BranchPresenter;
import com.example.tp_tea.ui.Branch.Location;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import retrofit2.Retrofit;

public interface MenuContact {
    // the branch contact is for connect the view, model and presenter
    public interface Presenter {
        void CallAPI();

        void GetResponseSuccess(List<Menu> datas);

        void ShowMessage(String title, int code);
    }

    public interface Model {
        void loadMenus(MenuPresenter presenter);
    }

    public interface menuView {
        void addToView(List<Menu> menus);

        void ShowMessage(String title, String msg);
    }
}
