package com.example.tp_tea.ui.menu;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tp_tea.*;
import com.example.tp_tea.R;

import java.util.List;

public class MenuFragment extends Fragment implements MenuContact.menuView {
    // set variable
    private RecyclerView recyclerView;
    private MenuPresenter presenter;
    private MenuRepository repository;
    private MenuRecyclerViewAdapter adapter;
    private MessageModel message;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_menu, container, false);

        recyclerView = (RecyclerView) root.findViewById(R.id.menulist);
        repository = new MenuRepository();
        message = new MessageModel();
        presenter = new MenuPresenter(this, repository, message);

        // call the api to load the menu
        presenter.CallAPI();
        return root;
    }

    @Override
    public void addToView(List<Menu> menus) {
        // add the record to view using recycler
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(this.getContext(), DividerItemDecoration.VERTICAL));
        adapter = new MenuRecyclerViewAdapter(menus, presenter);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void ShowMessage(String title, String msg) {
        // show message by alert dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getContext());
        builder.setMessage(msg);
        builder.setTitle(title);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create();
        builder.show();
    }
}