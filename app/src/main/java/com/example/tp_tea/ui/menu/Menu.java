package com.example.tp_tea.ui.menu;

import com.google.gson.annotations.SerializedName;

public class Menu {
    // set the menu variable
    @SerializedName("_id")
    public String _id;
    @SerializedName("enName")
    public String enName;
    @SerializedName("cnName")
    public String cnName;
    @SerializedName("hot")
    public String hot;
    @SerializedName("ice")
    public String ice;

    public Menu(String _id, String enName, String cnName, String hot, String ice) {
        this._id = _id;
        this.enName = enName;
        this.cnName = cnName;
        this.hot = hot;
        this.ice = ice;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }

    public String getCnName() {
        return cnName;
    }

    public void setCnName(String cnName) {
        this.cnName = cnName;
    }

    public String getHot() {
        return hot;
    }

    public void setHot(String hot) {
        this.hot = hot;
    }

    public String getIce() {
        return ice;
    }

    public void setIce(String ice) {
        this.ice = ice;
    }
}
