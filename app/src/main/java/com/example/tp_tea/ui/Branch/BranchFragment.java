package com.example.tp_tea.ui.Branch;

import android.Manifest;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;

import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.PermissionChecker;
import androidx.fragment.app.Fragment;

import com.example.tp_tea.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.example.tp_tea.*;

public class BranchFragment extends Fragment implements BranchContact.mapView {
    // set variable
    private BranchPresenter branchPresenter;
    private BranchRepository branchRepository;
    private GoogleMap mMap;
    private boolean permissionDenied = false;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private MessageModel message;

    private OnMapReadyCallback callback = new OnMapReadyCallback() {

        /**
         * Manipulates the map once available.
         * This callback is triggered when the map is ready to be used.
         * This is where we can add markers or lines, add listeners or move the camera.
         * In this case, we just add a marker near Sydney, Australia.
         * If Google Play services is not installed on the device, the user will be prompted to
         * install it inside the SupportMapFragment. This method will only be triggered once the
         * user has installed Google Play services and returned to the app.
         */

        @Override
        public void onMapReady(GoogleMap googleMap) {
            // set the default date, which show on map
            mMap = googleMap;
            branchPresenter.onMapReady();
            enableMyLocation();
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_branch, container, false);

        branchRepository = new BranchRepository();
        message = new MessageModel();
        branchPresenter = new BranchPresenter(this, branchRepository, message);

        return root;
    }

    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this.getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            if (mMap != null) {
                mMap.setMyLocationEnabled(true);
            }
        } else {
            // Ask user get permission
            ActivityCompat.requestPermissions(this.getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    100);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment =
                (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        // check the map fragment is not null
        if (mapFragment != null) {
            mapFragment.getMapAsync(callback);
        }
    }

    @Override
    public void addScooter(String licensePlate, LatLng location) {
        // add the location maker on the map
        mMap.addMarker(new MarkerOptions().position(location).title(licensePlate));
    }

    @Override
    public void scrollMapTo(LatLng location) {
        // set move camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(location));
        // set the camera in the location and set the zoom is 11
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 11));
    }

    @Override
    public void showMessage(String title, String msg) {
        // show the message using the alert dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getContext());
        builder.setMessage(msg);
        builder.setTitle(title);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create();
        builder.show();
    }
}