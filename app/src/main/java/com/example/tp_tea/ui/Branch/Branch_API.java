package com.example.tp_tea.ui.Branch;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Branch_API {
    // the get function : to get all branch data from api
    @GET("/branch")
    public Call<List<Branch>> getBranches();
}
