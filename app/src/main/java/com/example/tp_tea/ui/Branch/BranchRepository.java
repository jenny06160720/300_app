package com.example.tp_tea.ui.Branch;

import android.util.Log;

import com.example.tp_tea.Connect_Path;

import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BranchRepository implements BranchContact.Model {
    @Override
    public void loadLocation(BranchPresenter presenter) {
        // connect the api
        Branch_API connect_path = Connect_Path.retrofit2.create(Branch_API.class);

        // get the date and save to save to call variable
        Call<List<Branch>> call = connect_path.getBranches();

        call.enqueue(new Callback<List<Branch>>() {
            @Override
            public void onResponse(Call<List<Branch>> call, Response<List<Branch>> response) {
                // when the code is 200 it will display the locations on the map
                if (response.code() == 200) {
                    if (Locale.getDefault().getDisplayLanguage().equals("中文")) {
                        // when the application language is chinese
                        for (int i = 0; i < response.body().size(); i++) {
                            // set the marker
                            Location location = new Location(Double.parseDouble((response.body()).get(i).lat), Double.parseDouble((response.body()).get(i).lng));
                            presenter.getLocation((response.body()).get(i).chlocation, location);
                        }

                    } else {
                        // when the application language is not chinese, it will display text to be English
                        for (int i = 0; i < response.body().size(); i++) {
                            // set the marker
                            Location location = new Location(Double.parseDouble((response.body()).get(i).lat), Double.parseDouble((response.body()).get(i).lng));
                            presenter.getLocation((response.body()).get(i).enlocation, location);
                        }
                    }
                } else {
                    // display the error message
                    presenter.showMessage("Get Branch Information", response.code());
                }
            }

            @Override
            public void onFailure(Call<List<Branch>> call, Throwable t) {
                // display the error message
                presenter.showMessage("Get Branch Information", 0);
            }
        });
    }
}
