package com.example.tp_tea.ui.menu;

import com.example.tp_tea.ui.Branch.Branch;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Menu_API {
    // the get function : to get all branch data from api
    @GET("/menu")
    public Call<List<Menu>> getMenu();
}
