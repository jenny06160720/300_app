package com.example.tp_tea.ui.menu;

import android.util.Log;

import com.example.tp_tea.Connect_Path;
import com.example.tp_tea.ui.Branch.Branch;
import com.example.tp_tea.ui.Branch.BranchContact;
import com.example.tp_tea.ui.Branch.BranchPresenter;
import com.example.tp_tea.ui.Branch.Branch_API;
import com.example.tp_tea.ui.Branch.Location;

import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MenuRepository implements MenuContact.Model {
    @Override
    public void loadMenus(MenuPresenter presenter) {
        // connect the api
        Menu_API connect_path = Connect_Path.retrofit2.create(Menu_API.class);

        // get the date and save to save to call variable
        Call<List<Menu>> call = connect_path.getMenu();

        call.enqueue(new Callback<List<Menu>>() {
            @Override
            public void onResponse(Call<List<Menu>> call, Response<List<Menu>> response) {
                // when the code is 200 it will display the locations on the map
                if (response.code() == 200) {
                    presenter.GetResponseSuccess(response.body());
                } else {
                    // display the error message
                    presenter.ShowMessage("Get Menu Information", response.code());
                }
            }

            @Override
            public void onFailure(Call<List<Menu>> call, Throwable t) {
                // display the error message
                presenter.ShowMessage("Get Menu Information", 0);
            }
        });
    }
}
