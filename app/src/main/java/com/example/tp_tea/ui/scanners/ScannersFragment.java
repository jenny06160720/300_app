package com.example.tp_tea.ui.scanners;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.example.tp_tea.R;
import com.google.zxing.Result;

import java.util.Locale;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScannersFragment extends Fragment implements ZXingScannerView.ResultHandler {
    // set variable
    ZXingScannerView scannerView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_scanners, container, false);

        scannerView = root.findViewById(R.id.scanner);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                ActivityCompat.checkSelfPermission(this.getContext()
                        , Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this.getActivity(), new String[]{Manifest.permission.CAMERA},
                    100);
            openQRCamera();
        } else {
            openQRCamera();
        }
        return root;
    }

    private void openQRCamera() {
        // open the QR camera
        scannerView.setResultHandler(this);
        scannerView.startCamera();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // check the request code and grant result
        if (requestCode == 100 && grantResults[0] == 0) {
            openQRCamera();
        }
    }

    @Override
    public void handleResult(Result url) {
        if (Patterns.WEB_URL.matcher(url.getText()).matches()) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url.getText()));
            startActivity(browserIntent);
        } else {
            if (Locale.getDefault().getDisplayLanguage().equals("中文")) {
                showMessage("掃描器資訊", "連結無效");
            }else{
                showMessage("Scanner Information", "The url format is wrong!");
            }
        }
    }

    @Override
    public void onStop() {
        scannerView.stopCamera();
        super.onStop();
    }

    public void showMessage(String title, String msg) {
        // show the message using the alert dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getContext());
        builder.setMessage(msg);
        builder.setTitle(title);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                // call the open QR code camera function
                openQRCamera();
            }
        });
        builder.create();
        builder.show();
    }

}