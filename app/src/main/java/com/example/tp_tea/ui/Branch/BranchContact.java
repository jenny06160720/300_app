package com.example.tp_tea.ui.Branch;

import com.google.android.gms.maps.model.LatLng;

public interface BranchContact {
    // the branch contact is for connect the view, model and presenter
    interface Presenter {
        void onMapReady();

        void getLocation(String licensePlate, Location location);

        void showMessage(String title, int code);
    }

    interface Model {
        void loadLocation(BranchPresenter presenter);
    }

    interface mapView {
        void addScooter(String licensePlate, LatLng location);

        void scrollMapTo(LatLng location);

        void showMessage(String title, String message);
    }
}
