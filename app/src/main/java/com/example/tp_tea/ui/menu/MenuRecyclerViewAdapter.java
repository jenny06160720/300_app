package com.example.tp_tea.ui.menu;

import com.example.tp_tea.ui.menu.MenuRecyclerViewAdapter.ViewHolder;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tp_tea.R;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Locale;

public class MenuRecyclerViewAdapter extends RecyclerView.Adapter<MenuRecyclerViewAdapter.ViewHolder> {
    // set variable
    private List<Menu> menuData;
    private MenuPresenter presenter;

    public MenuRecyclerViewAdapter(List<Menu> menuData, MenuPresenter presenter) {
        this.menuData = menuData;
        this.presenter = presenter;
    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_menu, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Menu menu = menuData.get(position);
        // set the data for output
        String data = "";

        // check the drive language
        if (Locale.getDefault().getDisplayLanguage().equals("中文")) {
            data = "\n" + menu.cnName + "\n";
            data += "飲品可選：";
            if (menu.hot.equals("true") && menu.ice.equals("true")) {
                data += "熱/凍";
            } else if (menu.hot.equals("true")) {
                data += "熱";
            } else if (menu.ice.equals("true")) {
                data += "凍";
            }
        } else {
            data = menu.enName + "\n\n";
            data += "The drink can choose：";
            if (menu.hot.equals("true") && menu.ice.equals("true")) {
                data += "hot/ice";
            } else if (menu.hot.equals("true")) {
                data += "hoe";
            } else if (menu.ice.equals("true")) {
                data += "ice";
            }
        }

        data += "\n";

        holder.txtView.setText(data);

    }

    @Override
    public int getItemCount() {
        // return the size of the menu data list
        return menuData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtView;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            txtView = itemView.findViewById(R.id.txtMenu);
        }
    }
}
