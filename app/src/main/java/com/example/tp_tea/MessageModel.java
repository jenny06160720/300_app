package com.example.tp_tea;

public class MessageModel {
    // this class for saving the message
    public String title;
    public String msg;

    public MessageModel() {
    }

    public MessageModel(String title, String msg) {
        this.title = title;
        this.msg = msg;
    }

    public MessageModel(String title, int code) {
        this.title = title;
        this.msg = checkCodeNum(code);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String checkCodeNum(int codeNum){
        String message = null;
        if(codeNum == 100){
            // Informational responses, 100–199
            message = "100 Continue";
        }else if(codeNum == 101){
            message = "101 Switching Protocol";
        }else if(codeNum == 102){
            message = "102 Processing";
        }else if(codeNum == 103){
            message = "103 Early Hints";

        }else if(codeNum == 201){
            // Successful responses, 200–299
            message = "201 Created";
        }else if(codeNum == 202){
            message = "202 Accepted";
        }else if(codeNum == 203){
            message = "203 Non-Authoritative Information";
        }else if(codeNum == 204){
            message = "204 No Content";
        }else if(codeNum == 205){
            message = "205 Reset Content";
        }else if(codeNum == 206){
            message = "206 Partial Content";
        }else if(codeNum == 207){
            message = "207 Multi-Status (WebDAV)";
        }else if(codeNum == 208){
            message = "208 Multi-Status (WebDAV)";
        }else if(codeNum == 226){
            message = "226 IM Used (HTTP Delta encoding)";

        }else if(codeNum == 300){
            // Redirects, 300–399
            message = "300 Multiple Choice";
        }else if(codeNum == 301){
            message = "301 Moved Permanently";
        }else if(codeNum == 302){
            message = "302 Found";
        }else if(codeNum == 303){
            message = "303 See Other";
        }else if(codeNum == 304){
            message = "304 Not Modified";
        }else if(codeNum == 305){
            message = "305 Use Proxy";
        }else if(codeNum == 306){
            message = "306 unused";
        }else if(codeNum == 307){
            message = "307 Temporary Redirect";
        }else if(codeNum == 308){
            message = "308 Permanent Redirect";

        }else if(codeNum == 400){
            // Client errors, 400–499
            message = "400 Bad Request";
        }else if(codeNum == 401){
            message = "401 Unauthorized";
        }else if(codeNum == 402){
            message = "402 Payment Required";
        }else if(codeNum == 403){
            message = "403 Forbidden";
        }else if(codeNum == 404){
            message = "404 Not Found";
        }else if(codeNum == 405){
            message = "405 Method Not Allowed";
        }else if(codeNum == 406){
            message = "406 Not Acceptable";
        }else if(codeNum == 407){
            message = "407 Proxy Authentication Required";
        }else if(codeNum == 408){
            message = "408 Request Timeout";
        }else if(codeNum == 409){
            message = "409 Conflictt";
        }else if(codeNum == 410){
            message = "410 Gone";
        }else if(codeNum == 411){
            message = "411 Length Required";
        }else if(codeNum == 412){
            message = "412 Precondition Failed";
        }else if(codeNum == 413){
            message = "413 Payload Too Large";
        }else if(codeNum == 414){
            message = "414 URI Too Long";
        }else if(codeNum == 415){
            message = "415 Unsupported Media Type";
        }else if(codeNum == 416){
            message = "416 Requested Range Not Satisfiable";
        }else if(codeNum == 417){
            message = "417 Expectation Failed";
        }else if(codeNum == 418){
            message = "418 I'm a teapot";
        }else if(codeNum == 421){
            message = "421 Misdirected Request";
        }else if(codeNum == 422){
            message = "422 Unprocessable Entity";
        }else if(codeNum == 423){
            message = "423 Locked";
        }else if(codeNum == 424){
            message = "424 Failed Dependency";
        }else if(codeNum == 426){
            message = "426 Upgrade Required";
        }else if(codeNum == 428){
            message = "428 Precondition Required";
        }else if(codeNum == 429){
            message = "429 Too Many Requests";
        }else if(codeNum == 431){
            message = "431 Request Header Fields Too Large";
        }else if(codeNum == 451){
            message = "451 Unavailable For Legal Reasons";

        }else if(codeNum == 500){
            // Server errors, 500–599
            message = "500 Internal Server Error";
        }else if(codeNum == 501){
            message = "501 Not Implemented";
        }else if(codeNum == 502){
            message = "502 Bad Gateway";
        }else if(codeNum == 503){
            message = "503 Service Unavailable";
        }else if(codeNum == 504){
            message = "504 Gateway Timeout";
        }else if(codeNum == 505){
            message = "505 HTTP Version Not Supported";
        }else if(codeNum == 506){
            message = "506 Variant Also Negotiates";
        }else if(codeNum == 507){
            message = "507 Insufficient Storage";
        }else if(codeNum == 508){
            message = "508 Loop Detected";
        }else if(codeNum == 510){
            message = "510 Not Extended";
        }else if(codeNum == 511){
            message = "511 Network Authentication Required";
        }else{
            message = "Fail to connect server";
        }
        return message;
    }
}
