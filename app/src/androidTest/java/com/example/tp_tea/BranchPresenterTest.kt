package com.example.tp_tea

import com.example.tp_tea.ui.Branch.BranchContact
import com.example.tp_tea.ui.Branch.BranchPresenter
import com.example.tp_tea.ui.Branch.Location
import io.mockk.MockKAnnotations
import io.mockk.clearAllMocks
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.mockk
import io.mockk.verify
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class BranchPresenterTest {
    private var presenter: BranchPresenter? = null

    @RelaxedMockK
    lateinit var mView: BranchContact.mapView

    @RelaxedMockK
    lateinit var model: BranchContact.Model

    @RelaxedMockK
    lateinit var messages: MessageModel

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        presenter = BranchPresenter(mView, model, messages);
    }

    @After
    fun tearDown() {
        clearAllMocks()
    }

    @Test
    fun onMapReady() {
        // Given
        // When
        presenter?.onMapReady()
        // Then
        verify(exactly = 1) {
            model.loadLocation(presenter)
        }
    }


    @Test
    fun getLocation() {
        // Given
        var licensePlate = "";
        var location = Location(0.12,1.22);

        // When
        presenter?.getLocation(licensePlate,location)
        // Then
    }

    @Test
    fun showMessage(){
        // Given
        var title = "";
        var code = 404;

        // When
        title?.let{presenter?.showMessage(title,code)}
        // Then
        Assert.assertEquals(presenter?.title,"")
    }
}