package com.example.tp_tea

import android.content.Context
import android.content.Intent
import com.example.tp_tea.UI_Test.WelcomeContract
import com.example.tp_tea.UI_Test.WelcomePresenter
import io.mockk.*
import io.mockk.impl.annotations.RelaxedMockK
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotSame
import org.junit.After
import org.junit.Before
import org.junit.Test

class WelcomePresenterTest {
    private var presenter: WelcomePresenter? = null

    @RelaxedMockK
    lateinit var mView: WelcomeContract.MainView

    @RelaxedMockK
    lateinit var context: Context

    @Before
    fun setUp(){
        MockKAnnotations.init(this)
        presenter = WelcomePresenter(mView,context);
    }

    @After
    fun tearDown(){
        clearAllMocks()
    }

    @Test
    fun test_goToMainPage(){
        // Given
        var mockKIntents = slot<Intent>()

        // When
        presenter?.goToMainPage()

        // Then
        verify (exactly = 1){
            mView.goToMainPage(capture(mockKIntents))
        }
    }

    @Test
    fun test_goToMainPage_success(){
        // Given
        var mockKIntents = slot<Intent>()

        // When
        presenter?.goToMainPage()

        // Then
        verify (exactly = 1){
            mView.goToMainPage(capture(mockKIntents))
        }

        assertEquals(mockKIntents.captured.toString(),
            "Intent { cmp=/com.example.tp_tea.MainPage.MainActivity }")
    }

    @Test
    fun test_goToMainPage_CompareNoTEqual(){
        // Given
        var mockKIntents = slot<Intent>()

        // When
        presenter?.goToMainPage()

        // Then
        verify (exactly = 1){
            mView.goToMainPage(capture(mockKIntents))
        }

        assertNotSame(mockKIntents.captured.toString(),
            "Intent { cmp=/com.example.tp_tea.animation.WelcomeActivity }")
    }
}