package com.example.tp_tea

import android.content.Context
import android.content.Intent
import com.example.tp_tea.MainPage.MainContract
import com.example.tp_tea.MainPage.MainPresenter
import io.mockk.MockKAnnotations
import io.mockk.clearAllMocks
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.slot
import io.mockk.verify
import junit.framework.Assert
import org.junit.After
import org.junit.Before
import org.junit.Test

class MainPresenterTest {
    private var presenter: MainPresenter? = null

    @RelaxedMockK
    lateinit var mView: MainContract.MainView

    @RelaxedMockK
    lateinit var context: Context

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        presenter = MainPresenter(mView, context);
    }

    @After
    fun tearDown() {
        clearAllMocks()
    }

    @Test
    fun test_goToBranch() {
        // Given
        var mockKIntents = slot<Intent>()

        // When
        presenter?.goToBranch()

        // Then
        verify(exactly = 1) {
            mView.goToBranch(capture(mockKIntents))
        }
    }

    @Test
    fun test_goToBranch_success() {
        // Given
        var mockKIntents = slot<Intent>()

        // When
        presenter?.goToBranch()

        // Then
        verify(exactly = 1) {
            mView.goToBranch(capture(mockKIntents))
        }

        Assert.assertEquals(
            mockKIntents.captured.toString(),
            "Intent { cmp=/com.example.tp_tea.Tea_Navigation }"
        )
    }

    @Test
    fun test_goToBranch_CompareNotEqual() {
        // Given
        var mockKIntents = slot<Intent>()

        // When
        presenter?.goToBranch()

        // Then
        verify(exactly = 1) {
            mView.goToBranch(capture(mockKIntents))
        }

        Assert.assertNotSame(
            mockKIntents.captured.toString(),
            "Intent { cmp=/com.example.tp_tea.MainPage.MainActivity }"
        )

    }
}