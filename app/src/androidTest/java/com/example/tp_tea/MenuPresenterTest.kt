package com.example.tp_tea

import com.example.tp_tea.ui.menu.Menu
import com.example.tp_tea.ui.menu.MenuContact
import com.example.tp_tea.ui.menu.MenuPresenter
import io.mockk.*
import io.mockk.impl.annotations.RelaxedMockK
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class MenuPresenterTest {
    private var presenter: MenuPresenter? = null

    @RelaxedMockK
    lateinit var view: MenuContact.menuView

    @RelaxedMockK
    lateinit var model: MenuContact.Model

    @RelaxedMockK
    lateinit var message: MessageModel

    @Before
    fun setUp(){
        MockKAnnotations.init(this)
        presenter = MenuPresenter(view,model,message);
    }

    @After
    fun tearDown(){
        clearAllMocks()
    }

    @Test
    fun test_CallAPI(){
        // Given
        // When
        presenter?.CallAPI()
        // Then
        verify (exactly = 1){
            model.loadMenus(presenter)
        }
    }

    @Test
    fun test_GetResponseSuccess(){
        // Given
        var mockDate = mockk<List<Menu>>()
        // When
        presenter?.GetResponseSuccess(mockDate)
        // Then
        verify (exactly = 1){
            view.addToView(mockDate)
        }
    }

    @Test
    fun test_ShowMessage(){
        // Given
        var title = ""
        var code = 404
        // When
        code?.let{presenter?.ShowMessage(title,code)}
        // Then
        Assert.assertEquals(presenter?.msg,presenter?.messageModel?.checkCodeNum(code))
    }
}